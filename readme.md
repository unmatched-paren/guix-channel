
# ('s GNU Guix Channel
Currently offers the following packages:

- **emacs-zig-mode**: Adds syntax highlighting and indenting for Zig to Emacs.
- **helix**: A post-modern text editor.
- **i3-autotiling**: Automatically tile i3 and Sway layouts.
- **utop**: A fixed version of utop, an improved OCaml toplevel.

## Adding to Guix
If you already know what you're doing, here's the introduction information:
- Introduction commit: **8df545bcf04062bc197fb0d2bfb21edc6b76026f**
- Fingerprint: **0B83 7250 5DAF 9C01 A32C  3E4C DC2B ECE8 3E41 C67A**

If you don't, open up *~/.config/guix/channels.scm* in your favourite editor. Add the 
following:
``` scheme
(cons* (channel
        (name 'paren)
        (url "https://codeberg.org/unmatched-paren/guix-channel.git")
        (branch "origin/root")
        (introduction
         (make-channel-introduction
          "8df545bcf04062bc197fb0d2bfb21edc6b76026f"
          (openpgp-fingerprint
           "0B83 7250 5DAF 9C01 A32C  3E4C DC2B ECE8 3E41 C67A"))))
       %default-channels)
```

This will prepend this channel to your channel list. If there's another channel you want, add 
the (channel) declaration from its README or web page inside the cons*, don't add a second 
cons*:
``` scheme
(cons* (channel
        (name 'paren)
        (url "https://codeberg.org/unmatched-paren/guix-channel.git")
        (branch "origin/root")
        (introduction
         (make-channel-introduction
          "8df545bcf04062bc197fb0d2bfb21edc6b76026f"
          (openpgp-fingerprint
           "0B83 7250 5DAF 9C01 A32C  3E4C DC2B ECE8 3E41 C67A"))))
       (channel 
         #| the other channel's data goes here |#)
       %default-channels)
```