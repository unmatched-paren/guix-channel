
(define-module (paren packages c)
  #:use-module (guix build-system gnu)
  #:use-module (guix gexp)
  #:use-module (guix git-download)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix packages)
  #:use-module (paren packages qbe))

(define-public cproc
  (let ((commit "995d5b48b15779b9561aef59e035e34ef5c56839")
		(revision "0"))
    (package
	  (name "cproc")
	  (version (git-version "0.0.0" revision commit))
	  (source
		(origin
		  (method git-fetch)
		  (uri (git-reference
				 (url "https://git.sr.ht/~mcf/cproc")
				 (commit commit)))
		  (sha256
			(base32 "13vl22f5arrhcamdragj1cwwbs5bqcfd25idv3gh11g9bc39xm66"))))
	  (build-system gnu-build-system)
	  (arguments
		`(#:make-flags ,#~(list "CC=gcc" (string-append "PREFIX=" #$output))
		  #:phases
		  (modify-phases %standard-phases
			(delete 'configure))))
	  (native-inputs
		(list qbe))
	  (home-page "https://git.sr.ht/~mcf/cproc")
	  (synopsis "cproc is a C11 compiler using QBE as a backend.")
	  (description "cproc is a C11 compiler using QBE as a backend, released under the ISC license. Some C23 features and GNU C extensions are also implemented. There is still much to do, but it currently implements most of the language and is capable of building software including itself, mcpp, gcc 4.7, binutils, and more.")
	  (license license:isc))))

cproc
