;; SPDX-License-Identifier: GPL-3.0-or-later
;; See <https://spdx.org/licenses>.
;; Some code may be lifted from GNU Guix itself.
;; See <https://git.sv.gnu.org/cgit/guix.git/tree/gnu/packages/emacs-xyz.scm>
;; for the original license attributions.

(define-module (paren packages emacs-xyz)
  #:use-module (gnu packages emacs-xyz)
  #:use-module (guix build-system emacs)
  #:use-module (guix git-download)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix packages))

(define-public emacs-zig-mode
  ;; From 2021-12-27.
  ;; No releases available.
  (let ((commit "1ef8a13b93b3bdd20f86727b3a71572b02c421ef")
        (revision "0"))
    (package
     (name "emacs-zig-mode")
     (version (git-version "0.0.0" revision commit))
     (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/ziglang/zig-mode")
                    (commit commit)))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "06qgphf7341jyyzq2j9qpac6g7xj61kkvx70j82xg89r1ckrd37w"))))
     (build-system emacs-build-system)
     (propagated-inputs
      (list emacs-with-editor))
     (home-page "https://github.com/ziglang/zig-mode")
     (synopsis "Emacs major mode for the Zig programming language.")
     (description "This package adds a major mode for editing Zig code files. It provides automatic indentation and syntax highlighting.")
     (license license:gpl3+))))

(define-public emacs-ligature
  ;; From 2021-12-29.
  ;; No releases available.
  (let ((commit "c0e696a88824be6afa1f33fad548d36f96801d8e")
	(revision "0"))
    (package
     (name "emacs-ligature")
     (version (git-version "0.0.0" revision commit))
     (source (origin
	      (method git-fetch)
	      (uri (git-reference
		    (url "https://github.com/mickeynp/ligature.el")
		    (commit commit)))
	      (file-name (git-file-name name version))
	      (sha256
	       (base32 "10k7dwj0z0jckjla44icq1wcaw1jm4wnsvyzv8s1kf9nn6pb6gp8"))))
     (build-system emacs-build-system)
     (propagated-inputs
      (list emacs-with-editor))
     (home-page "https://github.com/mickeynp/ligature.el")
     (synopsis "Font Ligatures for Emacs")
     (description "This package adds a minor mode to display font ligatures in Emacs.")
     (license license:gpl3+))))
