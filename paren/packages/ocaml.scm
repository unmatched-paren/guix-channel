;; SPDX-License-Identifier: GPL-3.0-or-later
;; See <https://spdx.org/licenses>.
;; Some code may be lifted from GNU Guix itself;
;; see the files in <https://git.sv.gnu.org/cgit/guix.git/tree>
;; for the original attributions.

(define-module (paren packages ocaml)
  #:use-module (guix build-system dune)
  #:use-module (guix gexp)
  #:use-module (guix git-download)
  #:use-module (guix licenses)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix packages)
  #:use-module (gnu packages ocaml))

(define-public ocaml-utop
  (package
   (name "ocaml-utop")
   (version "2.9.0")
   (source
    (origin
     (method git-fetch)
     (uri (git-reference
	   (url "https://github.com/ocaml-community/utop")
	   (commit version)))
     (sha256
      (base32 "1mdpqc1b67p5rm2jsbwy0gjjgdlfqcakjyh1cwdj959ykz4zy9ld"))))
   (build-system dune-build-system)
   (arguments
    `(#:test-target "."
      #:phases 
		   ,#~(modify-phases %standard-phases
		     (add-before 'build 'patch-version
		       (lambda _
			 (substitute* "src/lib/uTop.ml"
		           (("%%VERSION%%") #$version))))
		     (add-after 'install 'wrap-program
		       (lambda* (#:key inputs outputs #:allow-other-keys)
			 (let* ((out (assoc-ref outputs "out"))
				(lwt (assoc-ref inputs "ocaml-lwt"))
			   (lambda-term (assoc-ref inputs "ocaml-lambda-term")))
			   (wrap-program (string-append out "/bin/utop")
			     `("LD_LIBRARY_PATH" ":" prefix (,(string-append lwt "/lib/ocaml/site-lib/stublibs/") ,(string-append lambda-term "/lib/ocaml/site-lib/stublibs/"))))))))))
   (native-inputs
    (list ocaml-cppo))
   (propagated-inputs
    (list ocaml-lambda-term ocaml-lwt ocaml-lwt-react ocaml-camomile
	  ocaml-react))
   ;(properties `((ocaml4.07-variant . ,(delay ocaml4.07-utop))))
   (home-page "https://github.com/ocaml-community/utop")
   (synopsis "Improved interface to the OCaml toplevel")
   (description "UTop is an improved toplevel for OCaml. It can run in a terminal or in Emacs. It supports line editing, history, real-time and context-sensitive completion, colours, and more.")
   (license license:bsd-3)))
