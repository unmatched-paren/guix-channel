;; SPDX-License-Identifier: GPL-3.0-or-later
;; See <https://spdx.org/licenses>.
;; Some code may be lifted from GNU Guix itself;
;; see the files in <https://git.sv.gnu.org/cgit/guix.git/tree>
;; for the original attributions.

(define-module (paren packages vim)
  #:use-module (guix build-system cmake)
  #:use-module (guix build-system gnu)
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix packages)
  #:use-module (guix utils)
  #:use-module (gnu packages base)
  #:use-module (gnu packages gettext)
  #:use-module (gnu packages gperf)
  #:use-module (gnu packages jemalloc)
  #:use-module (gnu packages libevent)
  #:use-module (gnu packages lua)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages serialization)
  #:use-module (gnu packages terminals)
  #:use-module (paren packages tree-sitter))

(define-public neovim-lua-libmpack
  (package 
    (inherit libmpack)
    (name "neovim-lua-libmpack")
    (version "1.0.8")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/libmpack/libmpack-lua")
             (commit version)))
       (file-name (git-file-name name version))
       (sha256
        (base32 "1ijvzgq5hvib03w5rghv31wi7byamwg7qdx5pawvhvnflaii8ivw"))))
    (build-system gnu-build-system)
    (arguments
     `(;; FIXME: tests require "busted", which is not yet available in Guix.
       #:tests? #f
       #:test-target "test"
       #:make-flags
       (let* ((lua-version ,(package-version lua))
              (lua-major+minor ,(version-major+minor (package-version lua))))
         (list "CC=gcc"
               "FETCH=echo"  ; don't fetch anything from the web
               "UNTGZ=echo"  ; and don't try to unpack it
               "USE_SYSTEM_LUA=yes"
               (string-append "MPACK_LUA_VERSION=" lua-version)
               (string-append "MPACK_LUA_VERSION_NOPATCH=" lua-major+minor)
               (string-append "PREFIX=" (assoc-ref %outputs "out"))
               (string-append "LUA_CMOD_INSTALLDIR="
                              (assoc-ref %outputs "out")
                              "/lib/lua" lua-major+minor)))
       #:phases
       (modify-phases %standard-phases
         (delete 'configure)
         (add-after 'unpack 'unpack-mpack-sources
           (lambda* (#:key inputs #:allow-other-keys)
             ;; This is broken because mpack-src is not a file, but all
             ;; prerequisites are added to the inputs of the gcc invocation.
	     (substitute* "Makefile"
               (("\\$\\(MPACK\\): mpack-src") "$(MPACK): "))
             (copy-recursively (assoc-ref inputs "libmpack")
                               "mpack-src"))))))
    (inputs (list lua))
    (native-inputs 
      `(("libmpack" ,(package-source libmpack)) 
	("pkg-config" ,pkg-config)))
    (home-page "https://github.com/libmpack/libmpack-lua")
    (synopsis "Lua bindings for the libmpack binary serialization library")))

(define-public neovim-lua5.1-libmpack
  (package 
    (inherit neovim-lua-libmpack)
    (name "neovim-lua5.1-libmpack")
    (arguments
     (substitute-keyword-arguments (package-arguments lua-libmpack)
       ((#:make-flags flags)
        `(let* ((lua-version ,(package-version lua-5.1))
                (lua-major+minor ,(version-major+minor (package-version lua-5.1))))
           (list "CC=gcc"
                 "USE_SYSTEM_LUA=yes"
                 (string-append "MPACK_LUA_VERSION=" lua-version)
                 (string-append "MPACK_LUA_VERSION_NOPATCH=" lua-major+minor)
                 (string-append "PREFIX=" (assoc-ref %outputs "out"))
                 (string-append "LUA_CMOD_INSTALLDIR="
                                (assoc-ref %outputs "out")
                                "/lib/lua" lua-major+minor))))))
    (inputs (list lua-5.1))))

(define (make-neovim-lua-lpeg name lua)
  (package
    (name name)
    (version "1.0.2")
    (source (origin
              (method url-fetch)
              (uri (string-append "http://www.inf.puc-rio.br/~roberto/lpeg/lpeg-"
                                  version ".tar.gz"))
              (sha256
               (base32 "1zjzl7acvcdavmcg5l7wi12jd4rh95q9pl5aiww7hv0v0mv6bmj8"))))
    (build-system gnu-build-system)
    (arguments
     `(#:phases
       (modify-phases %standard-phases
         (delete 'configure)
         ;; `make install` isn't available, so we have to do it manually
         (replace 'install
           (lambda* (#:key outputs #:allow-other-keys)
             (let ((out (assoc-ref outputs "out"))
                   (lua-version ,(version-major+minor (package-version lua))))
               (install-file "lpeg.so"
                             (string-append out "/lib/lua" lua-version))
               (install-file "re.lua"
                             (string-append out "/share/lua" lua-version))
               (rename-file
                 (string-append out "/share/lua" lua-version "/re.lua")
                 (string-append out "/share/lua" lua-version "/lpeg.lua"))))))
       #:test-target "test"))
    (inputs (list lua))
    (synopsis "Pattern-matching library for Lua")
    (description
     "LPeg is a pattern-matching library for Lua, based on Parsing Expression
Grammars (PEGs).")
    (home-page "http://www.inf.puc-rio.br/~roberto/lpeg")
    (license license:expat)))

(define-public neovim-lua5.1-lpeg
  (make-neovim-lua-lpeg "neovim-lua5.1-lpeg" lua-5.1))

(define-public neovim
  (package
    (name "neovim")
    (version "0.6.1")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
	       (url "https://github.com/neovim/neovim")
	       (commit (string-append "v" version))))
	      (file-name (git-file-name name version))
        (sha256
          (base32 "10p6lg5yv9n6wcwdprwvvi56dfcm4wsj54nm0invyx3mhf7374lx"))))
    (build-system cmake-build-system)
    (arguments
      `(#:modules ((srfi srfi-26)
                   (guix build cmake-build-system)
                   (guix build utils)
		   #|(guix utils)|#)
	#| TODO: try to get this working
	#:configure-flags (if (not (member %current-system 
					   (package-supported-systems luajit)))
			    '("-DPREFER_LUA:BOOL=YES"))
	|#
        #:phases (modify-phases %standard-phases
                  (add-after 'unpack 'set-lua-paths
                    (lambda* (#:key inputs #:allow-other-keys)
                      (let* ((lua-version "5.1")
                             (lua-cpath-spec
                               (lambda (prefix)
                                 (let ((path (string-append prefix "/lib/lua" lua-version)))
                                   (string-append path "/?.so;" path "/?/?.so"))))
                             (lua-path-spec
                               (lambda (prefix)
                                 (let ((path (string-append prefix "/share/lua" lua-version)))
                                   (string-append path "/?.lua" path "/?/?.lua"))))
                             (lua-inputs (map (cute assoc-ref inputs <>)
                                              '("lua"
                                                "lua-luv"
                                                "lua-lpeg"
                                                "lua-bitop"
                                                "lua-libmpack"))))
                         (setenv "LUA_PATH"
                           (string-join (map lua-path-spec lua-inputs) ";"))
                         (setenv "LUA_CPATH"
                           (string-join (map lua-cpath-spec lua-inputs) ";")))))
                   (add-after 'unpack 'prevent-embedding-gcc
                     (lambda _
                       ;; nvim remembers its build options, including the compiler with
                       ;; its complete path.  This adds gcc to the closure of nvim, which
                       ;; doubles its size.  We remove the reference here. 
                       (substitute* "cmake/GetCompileFlags.cmake"
                         (("\\$\\{CMAKE_C_COMPILER\\}") "/gnu/store/.../bin/gcc")))))))
          (inputs
           `(("libuv" ,libuv)
             ("msgpack" ,msgpack)
             ("libtermkey" ,libtermkey)
             ("libvterm" ,libvterm)
             ("unibilium" ,unibilium)
             ("jemalloc" ,jemalloc)
             ("libiconv" ,libiconv)
             ("luajit" ,luajit)
	     ("lua" ,lua-5.1)
             ("lua-luv" ,lua5.1-luv)
             ("lua-lpeg" ,neovim-lua5.1-lpeg)
             ("lua-bitop" ,lua5.1-bitop)
             ("lua-libmpack" ,neovim-lua5.1-libmpack)
             ("tree-sitter" ,tree-sitter)))
	  (native-inputs
	    (list gettext-minimal gperf pkg-config))
          (home-page "https://neovim.io")
          (synopsis "Fork of Vim focused on extensibility and agility")
          (description "Neovim is a project that seeks to aggressively
refactor Vim in order to:
@itemize
@item Simplify maintenance and encourage contributions
@item Split the work between multiple developers
@item Enable advanced external UIs without modifications to the core
@item Improve extensibility with a new plugin architecture
@end itemize\n")
          ;; Neovim is licensed under the terms of the Apache 2.0 license,
          ;; except for parts that were contributed under the Vim license.
          (license (list license:asl2.0 license:vim))))
