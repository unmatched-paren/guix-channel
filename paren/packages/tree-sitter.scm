;; SPDX-License-Identifier: GPL-3.0-or-later
;; See <https://spdx.org/licenses>.
;; Some code may be lifted from GNU Guix itself;
;; see the files in <https://git.sv.gnu.org/cgit/guix.git/tree>
;; for the original attributions.

;; attribution: https://issues.guix.gnu.org/49946

(define-module (paren packages tree-sitter)
  #:use-module (guix build-system cargo)
  #:use-module (guix build-system gnu)
  #:use-module (guix gexp)
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix packages)
  #:use-module (guix utils)
  #:use-module (gnu packages crates-graphics)
  #:use-module (gnu packages crates-io)
  #:use-module (paren packages crates-io))

(define-public tree-sitter
  (package
    (name "tree-sitter")
    (version "0.20.4")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/tree-sitter/tree-sitter")
                    (commit (string-append "v" version))))
              (file-name (git-file-name name version))
              (sha256
               (base32 "1yldgdbf3l5l4ki52abdf81nwkcbvg219gwr3ydcjwfsg7hf7zhz"))))
    (build-system gnu-build-system)
    (arguments
     (list
      #:tests? #f ;No check target.
      #:phases
      #~(modify-phases %standard-phases
          (delete 'configure)
          (add-before 'build 'set-cc
            (lambda _
              (setenv "CC"
                      #$(cc-for-target))))
          (replace 'install
            (lambda* (#:key outputs #:allow-other-keys)
              (let* ((out (assoc-ref outputs "out")) (lib (string-append
                                                           out "/lib")))
                (setenv "PREFIX" out)
                (invoke "make" "install")))))))
    (home-page "https://tree-sitter.github.io/tree-sitter/")
    (synopsis "Incremental parsing system for programming tools")
    (description
     "Tree-sitter is a parser generator tool and an incremental
parsing library.  It can build a concrete syntax tree for a source file and
efficiently update the syntax tree as the source file is edited.

Tree-sitter aims to be:

@enumerate
@item General enough to parse any programming language.
@item Fast enough to parse on every keystroke in a text editor.
@item Robust enough to provide useful results even in the presence of syntax
errors.
@item Dependency-free so that the runtime library (which is written in pure C)
can be embedded in any application.
@end enumerate

This package includes the @code{libtree-sitter} runtime library.")
    (license license:expat)))

(define-public rust-tree-sitter-0.20
  (package
    (name "rust-tree-sitter")
    (version "0.20.4")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "tree-sitter" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
         (base32 "16p3kysfzfgd8nyagfs2l8jpfdhr5cdlg0kk0czmwm5cirzk4d2f"))))
    (build-system cargo-build-system)
    (arguments
     `(#:tests? #f  ;; Running tests misinterprets comments as doc-tests.
       #:cargo-inputs
       (("rust-cc" ,rust-cc-1)
        ("rust-lazy-static" ,rust-lazy-static-1)
        ("rust-regex" ,rust-regex-1)
        ("rust-spin" ,rust-spin-0.7))))
    (home-page "https://tree-sitter.github.io/tree-sitter/")
    (synopsis "Rust bindings to the Tree-sitter parsing library")
    (description "This package provides Rust bindings to the Tree-sitter
parsing library.")
    (license license:expat)))

(define-public tree-sitter-cli
  (package (inherit tree-sitter)
    (name "tree-sitter-cli")
    (build-system cargo-build-system)
    (arguments
     `(;; Running test requires downloading fixtures, see the
       ;; script/fetch-fixtures script.
       #:tests? #f
       ;; FIXME: Installing the sources for the tree-sitter Rust bindings
       ;; doesn't work out of the box due to tree-sitter having multiple
       ;; Rust packages in the same repository (bindings and CLI).
       #:install-source? #f
       #:cargo-inputs
       (("rust-ansi-term" ,rust-ansi-term-0.12)
        ("rust-anyhow" ,rust-anyhow-1)
        ("rust-atty" ,rust-atty-0.2)
	("rust-cc" ,rust-cc-1)
        ("rust-clap" ,rust-clap-2)
        ("rust-difference" ,rust-difference-2)
        ("rust-dirs" ,rust-dirs-3)
	("rust-glob" ,rust-glob-0.3)
        ("rust-html-escape" ,rust-html-escape-0.2)
	("rust-lazy-static" ,rust-lazy-static-1)
        ("rust-libloading" ,rust-libloading-0.7)
	("rust-log" ,rust-log-0.4)
	("rust-memchr" ,rust-memchr-2)
	("rust-once-cell" ,rust-once-cell-1)
	("rust-regex" ,rust-regex-1)
	("rust-regex-syntax" ,rust-regex-syntax-0.6)
        ("rust-rustc-hash" ,rust-rustc-hash-1)
        ("rust-serde" ,rust-serde-1)
	("rust-serde-json" ,rust-serde-json-1)
        ("rust-smallbitvec" ,rust-smallbitvec-2)
        ("rust-thiserror" ,rust-thiserror-1)
        ("rust-tiny-http" ,rust-tiny-http-0.8)
        ("rust-toml" ,rust-toml-0.5)
        ("rust-walkdir" ,rust-walkdir-2)
        ("rust-webbrowser" ,rust-webbrowser-0.5)
        ("rust-which" ,rust-which-4))
       #:cargo-development-inputs
       (("rust-ctor" ,rust-ctor-0.1)
	("rust-pretty-assertions" ,rust-pretty-assertions-0.7)
	("rust-rand" ,rust-rand-0.8)
	("rust-tempfile" ,rust-tempfile-3))
       #:phases
       (modify-phases %standard-phases
         (add-after 'unpack 'delete-cargo.lock
           (lambda _ (delete-file "Cargo.lock")))
         (replace 'install
           (lambda* (#:key outputs #:allow-other-keys)
             (let* ((out (assoc-ref outputs "out"))
                    (bin (string-append out "/bin"))
                    (lib (string-append out "/lib")))
               (mkdir-p bin)
               (install-file "target/release/tree-sitter" bin)))))))
    (synopsis "Incremental parsing system for programming tools")
    (description "Tree-sitter is a parser generator tool and an incremental
parsing library.  It can build a concrete syntax tree for a source file and
efficiently update the syntax tree as the source file is edited.

Tree-sitter aims to be:

@enumerate
@item General enough to parse any programming language.
@item Fast enough to parse on every keystroke in a text editor.
@item Robust enough to provide useful results even in the presence of syntax
errors.
@item Dependency-free so that the runtime library (which is written in pure C)
can be embedded in any application.
@end enumerate

This package includes the @command{tree-sitter} command-line tool.")
    (license license:expat)))
