
(define-module (paren packages gl)
  #:use-module (guix build-system python)
  #:use-module (guix git-download)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix packages)
  #:use-module (gnu packages python-xyz))

(define-public glad2
  (let ((commit "7a3c14c31df49b2f7f62e91d0ed4eb82379d7d6d")
	(revision "0"))
    (package
      (name "glad2")
      (version (git-version "0.1.36" revision commit))
      (source
	(origin
	  (method git-fetch)
	  (uri (git-reference
		 (url "https://github.com/Dav1dde/glad")
		 (commit commit)))
	  (sha256
	    (base32 "0gsa5w8fxw24g8m9djg5938lw323k50fl3zv8lb8zks2m0l3k7vx"))))
      (build-system python-build-system)
      (arguments
	`(#:phases
	  (modify-phases %standard-phases
	    (add-after 'install 'install-cmakelists.txt
	      (lambda* (#:key outputs #:allow-other-keys)
		(let* ((out (assoc-ref outputs "out"))
		       (share (string-append out "/share/" ,name)))
		  (install-file "cmake/CMakeLists.txt" share)))))))
      (inputs (list python-jinja2))
      (home-page "https://glad.sh")
      (synopsis "Multi-language GL/GLES/EGL/GLX/WGL/Vulkan loader generator")
      (description "GLAD 2 is an improved iteration of the GLAD graphics API loader-generator. Among the improvements are:
@enumerate
@item Better EGL, GLX, and WGL support
@item Vulkan support
@item Rust support
@item More generator options (e.g. an option to make GLAD header-only)
@item Better XML parsing
@item Better Web-Generator
@item Better CMake support
@item Better examples
@item Better CLI
@item Better loader
@item Better API
@end enumerate")
      (license license:expat))))
