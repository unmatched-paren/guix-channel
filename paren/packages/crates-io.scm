;; SPDX-License-Identifier: GPL-3.0-or-later
;; See <https://spdx.org/licenses>.
;; Some code may be lifted from GNU Guix itself;
;; see the files in <https://git.sv.gnu.org/cgit/guix.git/tree>
;; for the original attributions.

(define-module (paren packages crates-io)
  #:use-module (guix build-system cargo)
  #:use-module (guix download)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix packages)
  #:use-module (gnu packages crates-io))

(define-public rust-generator-0.7
  (package
    (name "rust-generator")
    (version "0.7.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "generator" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1vhj3f0rf4mlh5vz7pz5rxmgry1cc62x21mf9ld1r292m2f2gnf1"))))
    (build-system cargo-build-system)
    (arguments
      `(#:cargo-inputs
        (("rust-cc" ,rust-cc-1)
         ("rust-libc" ,rust-libc-0.2)
         ("rust-log" ,rust-log-0.4)
         ("rust-rustversion" ,rust-rustversion-1)
         ("rust-winapi" ,rust-winapi-0.3))))
    (home-page "https://github.com/Xudong-Huang/generator-rs.git")
    (synopsis "Stackfull Generator Library in Rust")
    (description "Stackfull Generator Library in Rust")
    (license (list license:expat license:asl2.0))))

(define-public rust-html-escape-0.2
  (package
    (name "rust-html-escape")
    (version "0.2.9")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "html-escape" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1dxw9lpckrqzzqgbkw64ckbajr4b7xxnjdn8adgzqf2mm40shvl1"))))
    (build-system cargo-build-system)
    (arguments
      `(#:cargo-inputs
        (("rust-utf8-width" ,rust-utf8-width-0.1))
        #:cargo-development-inputs
        (("rust-bencher" ,rust-bencher-0.1))))
    (home-page "https://magiclen.org/html-escape")
    (synopsis
      "This library is for encoding/escaping special characters in HTML and decoding/unescaping HTML entities as well.")
    (description
      "This library is for encoding/escaping special characters in HTML and
decoding/unescaping HTML entities as well.")
    (license license:expat)))

(define-public rust-spin-0.7
  (package
    (name "rust-spin")
    (version "0.7.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "spin" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0qjips9f6fsvkyd7wj3a4gzaqknn2q4kkb19957pl86im56pna0k"))))
    (build-system cargo-build-system)
    (arguments `(#:cargo-inputs (("rust-lock-api" ,rust-lock-api-0.4))))
    (home-page "https://github.com/mvdnes/spin-rs.git")
    (synopsis "Spin-based synchronization primitives")
    (description "Spin-based synchronization primitives")
    (license license:expat)))

(define-public rust-proptest-derive-0.2
  (package
    (name "rust-proptest-derive")
    (version "0.2.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "proptest-derive" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1m1jbydld98v3kfr32c1xqgmgkrcnym6x2rjjkjxzsg9vch9s785"))))
    (build-system cargo-build-system)
    (arguments
      `(#:cargo-inputs
        (("rust-proc-macro2" ,rust-proc-macro2-0.4)
         ("rust-quote" ,rust-quote-0.6)
         ("rust-syn" ,rust-syn-0.15))
        #:cargo-development-inputs
        (("rust-compiletest-rs" ,rust-compiletest-rs-0.3)
         ("rust-criterion" ,rust-criterion-0.2)
         ("rust-proptest" ,rust-proptest-0.10))))
    (home-page
      "https://altsysrq.github.io/proptest-book/proptest-derive/index.html")
    (synopsis "Custom-derive for the Arbitrary trait of proptest.
")
    (description "Custom-derive for the Arbitrary trait of proptest.")
    (license (list license:expat license:asl2.0))))

(define-public rust-windows-x86-64-gnu-0.32
  (package
    (name "rust-windows-x86-64-gnu")
    (version "0.32.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "windows-x86-64-gnu" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1g34xhcayig9sndq3555w95q6lr7jr839zxv6l365ijlfhpv24n9"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "https://github.com/microsoft/windows-rs")
    (synopsis "Code gen support for the windows crate")
    (description "Code gen support for the windows crate")
    (license (list license:expat license:asl2.0))))

(define-public rust-windows-x86-64-msvc-0.32
  (package
    (name "rust-windows-x86-64-msvc")
    (version "0.32.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "windows-x86-64-msvc" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "05l392h518dxn808dc1zkv6d0r9z38q68qqc0ix9fs9741v28jjh"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "https://github.com/microsoft/windows-rs")
    (synopsis "Code gen support for the windows crate")
    (description "Code gen support for the windows crate")
    (license (list license:expat license:asl2.0))))

(define-public rust-windows-i686-msvc-0.32
  (package
    (name "rust-windows-i686-msvc")
    (version "0.32.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "windows-i686-msvc" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0wj1wi01fc8hrasbakjcq8y5a7ynw9l2mcw08svmsq823axi2v0l"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "https://github.com/microsoft/windows-rs")
    (synopsis "Code gen support for the windows crate")
    (description "Code gen support for the windows crate")
    (license (list license:expat license:asl2.0))))

(define-public rust-windows-i686-gnu-0.32
  (package
    (name "rust-windows-i686-gnu")
    (version "0.32.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "windows-i686-gnu" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "05g6kpdfxwxnw2gn1nrd7bsf5997rci0k3h3nqby168ph5l1qwba"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "https://github.com/microsoft/windows-rs")
    (synopsis "Code gen support for the windows crate")
    (description "Code gen support for the windows crate")
    (license (list license:expat license:asl2.0))))

(define-public rust-windows-aarch64-msvc-0.32
  (package
    (name "rust-windows-aarch64-msvc")
    (version "0.32.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "windows-aarch64-msvc" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1x8bnafz15ksgpbjbgk1l1j2jx4rq4a2ylzcahb1jhy4n59jgsfq"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "https://github.com/microsoft/windows-rs")
    (synopsis "Code gen support for the windows crate")
    (description "Code gen support for the windows crate")
    (license (list license:expat license:asl2.0))))

(define-public rust-windows-sys-0.32
  (package
    (name "rust-windows-sys")
    (version "0.32.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "windows-sys" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1imdvrsbivgrvzb9261aa77ws391l24s3r1b0wna34jz31vf9xix"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs
        (("rust-windows-aarch64-msvc" ,rust-windows-aarch64-msvc-0.32)
         ("rust-windows-i686-gnu" ,rust-windows-i686-gnu-0.32)
         ("rust-windows-i686-msvc" ,rust-windows-i686-msvc-0.32)
         ("rust-windows-x86-64-gnu" ,rust-windows-x86-64-gnu-0.32)
         ("rust-windows-x86-64-msvc" ,rust-windows-x86-64-msvc-0.32))))
    (home-page "https://github.com/microsoft/windows-rs")
    (synopsis "Rust for Windows")
    (description "Rust for Windows")
    (license (list license:expat license:asl2.0))))

(define-public rust-thread-id-4
  (package
    (name "rust-thread-id")
    (version "4.0.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "thread-id" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0zvikdngp0950hi0jgiipr8l36rskk1wk7pc8cd43xr3g5if1psz"))))
    (build-system cargo-build-system)
    (arguments
      `(#:cargo-inputs
        (("rust-libc" ,rust-libc-0.2)
         ("rust-redox-syscall" ,rust-redox-syscall-0.2)
         ("rust-winapi" ,rust-winapi-0.3))))
    (home-page "https://github.com/ruuda/thread-id")
    (synopsis "Get a unique thread ID")
    (description "Get a unique thread ID")
    (license (list license:expat license:asl2.0))))

(define-public rust-parking-lot-core-0.9
  (package
    (name "rust-parking-lot-core")
    (version "0.9.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "parking-lot-core" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0m49xlpxyw0c65c3011zvgzn2slpviw494816d2a4g8lqh61w518"))))
    (build-system cargo-build-system)
    (arguments
      `(#:cargo-inputs
        (("rust-backtrace" ,rust-backtrace-0.3)
         ("rust-cfg-if" ,rust-cfg-if-1)
         ("rust-libc" ,rust-libc-0.2)
         ("rust-petgraph" ,rust-petgraph-0.5)
         ("rust-redox-syscall" ,rust-redox-syscall-0.2)
         ("rust-smallvec" ,rust-smallvec-1)
         ("rust-thread-id" ,rust-thread-id-4)
         ("rust-windows-sys" ,rust-windows-sys-0.32))))
    (home-page "https://github.com/Amanieu/parking_lot")
    (synopsis
      "An advanced API for creating custom synchronization primitives.")
    (description
      "An advanced API for creating custom synchronization primitives.")
    (license (list license:asl2.0 license:expat))))

(define-public rust-parking-lot-0.12
  (package
    (name "rust-parking-lot")
    (version "0.12.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "parking-lot" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0n7gp0cnfghglc370cxhawwfijvhj3wrjh8gdi8c06m6jcjfrxc7"))))
    (build-system cargo-build-system)
    (arguments
      `(#:cargo-inputs
        (("rust-lock-api" ,rust-lock-api-0.4)
         ("rust-parking-lot-core" ,rust-parking-lot-core-0.9))
        #:cargo-development-inputs
        (("rust-bincode" ,rust-bincode-1) ("rust-rand" ,rust-rand-0.8))))
    (home-page "https://github.com/Amanieu/parking_lot")
    (synopsis
      "More compact and efficient implementations of the standard synchronization primitives.")
    (description
      "More compact and efficient implementations of the standard synchronization
primitives.")
    (license (list license:asl2.0 license:expat))))

(define-public rust-unic-ucd-category-0.9
  (package
    (name "rust-unic-ucd-category")
    (version "0.9.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "unic-ucd-category" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1h4ixzplc2s441vc8mc4zxliw6qfqh1ziaiv8pa1pzpwyn8lb38v"))))
    (build-system cargo-build-system)
    (arguments
      `(#:cargo-inputs
        (("rust-matches" ,rust-matches-0.1)
         ("rust-unic-char-property" ,rust-unic-char-property-0.9)
         ("rust-unic-char-range" ,rust-unic-char-range-0.9)
         ("rust-unic-ucd-version" ,rust-unic-ucd-version-0.9))))
    (home-page "https://github.com/open-i18n/rust-unic/")
    (synopsis
      "UNIC â\x80\x94 Unicode Character Database â\x80\x94 General Category")
    (description
      "UNIC â\x80\x94 Unicode Character Database â\x80\x94 General Category")
    (license (list license:expat license:asl2.0))))

(define-public rust-unic-ucd-hangul-0.9
  (package
    (name "rust-unic-ucd-hangul")
    (version "0.9.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "unic-ucd-hangul" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0m001jjck7j34hsf6kw9aidzapms9hi175yv7r9f244hw68cc7gb"))))
    (build-system cargo-build-system)
    (arguments
      `(#:cargo-inputs (("rust-unic-ucd-version" ,rust-unic-ucd-version-0.9))))
    (home-page "https://github.com/open-i18n/rust-unic/")
    (synopsis
      "UNIC â\x80\x94 Unicode Character Database â\x80\x94 Hangul Syllable Composition & Decomposition")
    (description
      "UNIC â\x80\x94 Unicode Character Database â\x80\x94 Hangul Syllable Composition &
Decomposition")
    (license (list license:expat license:asl2.0))))

(define-public rust-unic-ucd-normal-0.9
  (package
    (name "rust-unic-ucd-normal")
    (version "0.9.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "unic-ucd-normal" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "044laqqf09xqv4gl27f328a2f780gkzabpar72qj4b90p1rxibl6"))))
    (build-system cargo-build-system)
    (arguments
      `(#:cargo-inputs
        (("rust-unic-char-property" ,rust-unic-char-property-0.9)
         ("rust-unic-char-range" ,rust-unic-char-range-0.9)
         ("rust-unic-ucd-category" ,rust-unic-ucd-category-0.9)
         ("rust-unic-ucd-hangul" ,rust-unic-ucd-hangul-0.9)
         ("rust-unic-ucd-version" ,rust-unic-ucd-version-0.9))
        #:cargo-development-inputs
        (("rust-unic-ucd-category" ,rust-unic-ucd-category-0.9))))
    (home-page "https://github.com/open-i18n/rust-unic/")
    (synopsis
      "UNIC â\x80\x94 Unicode Character Database â\x80\x94 Normalization Properties")
    (description
      "UNIC â\x80\x94 Unicode Character Database â\x80\x94 Normalization Properties")
    (license (list license:expat license:asl2.0))))

(define-public rust-unic-normal-0.9
  (package
    (name "rust-unic-normal")
    (version "0.9.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "unic-normal" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0qmsdf7b902mmaslhwww0hzmzqn26mzh7sraphl4dac96p9n97gh"))))
    (build-system cargo-build-system)
    (arguments
      `(#:cargo-inputs
        (("rust-unic-ucd-normal" ,rust-unic-ucd-normal-0.9))
        #:cargo-development-inputs
        (("rust-unic-ucd-version" ,rust-unic-ucd-version-0.9))))
    (home-page "https://github.com/open-i18n/rust-unic/")
    (synopsis "UNIC â\x80\x94 Unicode Normalization Forms")
    (description "UNIC â\x80\x94 Unicode Normalization Forms")
    (license (list license:expat license:asl2.0))))

(define-public rust-detone-1
  (package
    (name "rust-detone")
    (version "1.0.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "detone" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "052mnp8qjp71ypcv0ixvlw7isn1yv79qn05jrcfi9j2r70clq47p"))))
    (build-system cargo-build-system)
    (arguments
      `(#:cargo-development-inputs
        (("rust-unic-normal" ,rust-unic-normal-0.9))))
    (home-page "https://docs.rs/detone/")
    (synopsis "Decompose Vietnamese tone marks")
    (description "Decompose Vietnamese tone marks")
    (license (list license:expat license:asl2.0))))

(define-public rust-similar-2
  (package
    (name "rust-similar")
    (version "2.1.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "similar" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1lw33na01r35h09s47jqhjgz3m29wapl20f6ybsla5d1cfgrf91f"))))
    (build-system cargo-build-system)
    (arguments
      `(#:cargo-inputs
        (("rust-bstr" ,rust-bstr-0.2)
         ("rust-serde" ,rust-serde-1)
         ("rust-unicode-segmentation" ,rust-unicode-segmentation-1))
        #:cargo-development-inputs
        (("rust-console" ,rust-console-0.14)
         ("rust-insta" ,rust-insta-1)
         ("rust-serde-json" ,rust-serde-json-1))))
    (home-page "https://github.com/mitsuhiko/similar")
    (synopsis "A diff library for Rust")
    (description "This package provides a diff library for Rust")
    (license license:asl2.0)))

(define-public rust-crossterm-winapi-0.9
  (package
    (name "rust-crossterm-winapi")
    (version "0.9.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "crossterm-winapi" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "075z15gxm4rn5yywq46khbg29bf504ix0f06zq3hx8aa91db7q9a"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-winapi" ,rust-winapi-0.3))))
    (home-page "https://github.com/crossterm-rs/crossterm-winapi")
    (synopsis
      "WinAPI wrapper that provides some basic simple abstractions around common WinAPI calls")
    (description
      "WinAPI wrapper that provides some basic simple abstractions around common WinAPI
calls")
    (license license:expat)))

(define-public rust-crossterm-0.22
  (package
    (name "rust-crossterm")
    (version "0.22.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "crossterm" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0p1j2qfby343qf2isv7g78hrp7rkkk7qlg797jvx34a2dhq2amf8"))))
    (build-system cargo-build-system)
    (arguments
      `(#:cargo-inputs
        (("rust-bitflags" ,rust-bitflags-1)
         ("rust-crossterm-winapi" ,rust-crossterm-winapi-0.9)
         ("rust-futures-core" ,rust-futures-core-0.3)
         ("rust-libc" ,rust-libc-0.2)
         ("rust-mio" ,rust-mio-0.7)
         ("rust-parking-lot" ,rust-parking-lot-0.11)
         ("rust-serde" ,rust-serde-1)
         ("rust-signal-hook" ,rust-signal-hook-0.3)
         ("rust-signal-hook-mio" ,rust-signal-hook-mio-0.2)
         ("rust-winapi" ,rust-winapi-0.3))
        #:cargo-development-inputs
        (("rust-async-std" ,rust-async-std-1)
         ("rust-futures" ,rust-futures-0.3)
         ("rust-futures-timer" ,rust-futures-timer-3)
         ("rust-serde-json" ,rust-serde-json-1)
         ("rust-tokio" ,rust-tokio-1))))
    (home-page "https://github.com/crossterm-rs/crossterm")
    (synopsis "A crossplatform terminal library for manipulating terminals.")
    (description
      "This package provides a crossplatform terminal library for manipulating
terminals.")
    (license license:expat)))

(define-public rust-chardetng-0.1
  (package
    (name "rust-chardetng")
    (version "0.1.17")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "chardetng" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1spikjcnblwa5n1nnk46fxkwn86yfiqxgs47h4yaw23vbfvg1f0l"))))
    (build-system cargo-build-system)
    (arguments
      `(#:cargo-inputs
        (("rust-arrayvec" ,rust-arrayvec-0.5)
         ("rust-cfg-if" ,rust-cfg-if-1)
         ("rust-encoding-rs" ,rust-encoding-rs-0.8)
         ("rust-memchr" ,rust-memchr-2)
         ("rust-rayon" ,rust-rayon-1))
        #:cargo-development-inputs
        (("rust-detone" ,rust-detone-1))))
    (home-page "https://docs.rs/chardetng/")
    (synopsis "A character encoding detector for legacy Web content")
    (description
      "This package provides a character encoding detector for legacy Web content")
    (license (list license:asl2.0 license:expat))))

(define-public rust-signal-hook-tokio-0.3
  (package
    (name "rust-signal-hook-tokio")
    (version "0.3.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "signal-hook-tokio" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "07nggsi80jv39xisdk2r7cik7hx2d2qa2sivvqkpxqxidzvl2ci1"))))
    (build-system cargo-build-system)
    (arguments
      `(#:cargo-inputs
        (("rust-futures-core" ,rust-futures-core-0.3)
         ("rust-libc" ,rust-libc-0.2)
         ("rust-signal-hook" ,rust-signal-hook-0.3)
         ("rust-tokio" ,rust-tokio-1))
        #:cargo-development-inputs
        (("rust-futures" ,rust-futures-0.3)
         ("rust-serial-test" ,rust-serial-test-0.5)
         ("rust-tokio" ,rust-tokio-1))))
    (home-page "https://github.com/vorner/signal-hook")
    (synopsis "Tokio support for signal-hook")
    (description "Tokio support for signal-hook")
    (license (list license:asl2.0 license:expat))))

(define-public rust-etcetera-0.3
  (package
    (name "rust-etcetera")
    (version "0.3.2")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "etcetera" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1v1d2z2h1ay3skks0s4n833z275nkdf28d168cyq7ywl3vyh8sq1"))))
    (build-system cargo-build-system)
    (arguments
      `(#:cargo-inputs
        (("rust-cfg-if" ,rust-cfg-if-1)
         ("rust-dirs-next" ,rust-dirs-next-2)
         ("rust-thiserror" ,rust-thiserror-1))))
    (home-page "https://github.com/arzg/etcetera")
    (synopsis
      "An unopinionated library for obtaining configuration, data and cache directories")
    (description
      "This package provides an unopinionated library for obtaining configuration, data and cache directories.")
    (license (list license:expat license:asl2.0))))

(define-public rust-smartstring-0.2
  (package
    (name "rust-smartstring")
    (version "0.2.9")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "smartstring" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "16rc6n0p4r4aw6k6jxf2s37wyaijaa4pwpw7rqki7cn2q0qnmaii"))))
    (build-system cargo-build-system)
    (arguments
      `(#:cargo-inputs
        (("rust-arbitrary" ,rust-arbitrary-0.4)
         ("rust-proptest" ,rust-proptest-0.10)
         ("rust-serde" ,rust-serde-1)
         ("rust-static-assertions" ,rust-static-assertions-1))
        #:cargo-development-inputs
        (("rust-criterion" ,rust-criterion-0.3)
         ("rust-proptest" ,rust-proptest-0.10)
         ("rust-proptest-derive" ,rust-proptest-derive-0.2)
         ("rust-rand" ,rust-rand-0.7)
         ("rust-serde-test" ,rust-serde-test-1))))
    (home-page "https://github.com/bodil/smartstring")
    (synopsis "Compact inlined strings")
    (description "This package provides compact inlined strings.")
    (license license:mpl2.0
	     #| MPL 2.0 _or later_! Guix doesn't have or-laters for MPLs. |#)))

(define-public rust-slotmap-1
  (package
    (name "rust-slotmap")
    (version "1.0.6")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "slotmap" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0hhkvsc3x79c7fh97b3padjhssd19hzdyyiv291mr3qf3lk8xq71"))))
    (build-system cargo-build-system)
    (arguments
      `(#:cargo-inputs
        (("rust-serde" ,rust-serde-1)
         ("rust-version-check" ,rust-version-check-0.9))
        #:cargo-development-inputs
        (("rust-fxhash" ,rust-fxhash-0.2)
         ("rust-quickcheck" ,rust-quickcheck-0.9)
         ("rust-serde" ,rust-serde-1)
         ("rust-serde-derive" ,rust-serde-derive-1)
         ("rust-serde-json" ,rust-serde-json-1))))
    (home-page "https://github.com/orlp/slotmap")
    (synopsis "Slotmap data structure")
    (description "This package provides a slotmap data structure.")
    (license license:zlib)))

(define-public rust-smallvec-1.8
  (package
    (name "rust-smallvec")
    (version "1.8.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "smallvec" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "10zf4fn63p2d6sx8qap3jvyarcfw563308x3431hd4c34r35gpgj"))))
    (build-system cargo-build-system)
    (arguments
      `(#:cargo-inputs
        (("rust-arbitrary" ,rust-arbitrary-1) ("rust-serde" ,rust-serde-1))
        #:cargo-development-inputs
        (("rust-bincode" ,rust-bincode-1))))
    (home-page "https://github.com/servo/rust-smallvec")
    (synopsis
      "'Small vector' optimization")
    (description
      "This package provides a 'small vector' optimization to store up to a small number of items on the stack.")
    (license (list license:expat license:asl2.0))))

(define-public rust-smallbitvec-2
  (package
    (name "rust-smallbitvec")
    (version "2.5.1")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "smallbitvec" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32
         "0plrbldsjpwip3afbzd8fgrnvdhizcg5z4ncfqs4q6x4qjflzkkm"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-development-inputs
       (("rust-bit-vec" ,rust-bit-vec-0.4)
        ("rust-rand" ,rust-rand-0.4))))
    (home-page "https://github.com/servo/smallbitvec")
    (synopsis "A bit vector optimized for size and inline storage")
    (description "This package provides a bit vector optimized for size and
inline storage.")
    (license (list license:expat license:asl2.0))))

(define-public rust-ropey-1.3
  (package
    (name "rust-ropey")
    (version "1.3.2")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "ropey" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1hzyq8y5nzcfp74j0gi1a222p8xamyhv8n3igk9hiwyrpijsmfg6"))))
    (build-system cargo-build-system)
    (arguments
      `(#:cargo-inputs
        (("rust-smallvec" ,rust-smallvec-1))
        #:cargo-development-inputs
        (("rust-criterion" ,rust-criterion-0.3)
         ("rust-proptest" ,rust-proptest-0.9)
         ("rust-rand" ,rust-rand-0.7)
         ("rust-unicode-segmentation" ,rust-unicode-segmentation-1))))
    (home-page "https://github.com/cessen/ropey")
    (synopsis "A fast and robust text rope for Rust")
    (description "This package provides a fast and robust text rope for Rust.")
    (license license:expat)))

(define-public rust-futures-macro-0.3
  (package
    (name "rust-futures-macro")
    (version "0.3.21")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "futures-macro" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "04pmj5xfk5rdhlj69wc7w3zvdg3xardg8srig96lszrk00wf3h9k"))))
    (build-system cargo-build-system)
    (arguments
      `(#:cargo-inputs
        (("rust-proc-macro2" ,rust-proc-macro2-1)
         ("rust-quote" ,rust-quote-1)
         ("rust-syn" ,rust-syn-1))))
    (home-page "https://rust-lang.github.io/futures-rs")
    (synopsis "The futures-rs procedural macro implementations.
")
    (description "The futures-rs procedural macro implementations.")
    (license (list license:expat license:asl2.0))))

(define-public rust-futures-io-0.3
  (package
    (name "rust-futures-io")
    (version "0.3.21")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "futures-io" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0swn29fysas36ikk5aw55104fi98117amvgxw9g96pjs5ab4ah7w"))))
    (build-system cargo-build-system)
    (home-page "https://rust-lang.github.io/futures-rs")
    (synopsis
      "The `AsyncRead`, `AsyncWrite`, `AsyncSeek`, and `AsyncBufRead` traits for the futures-rs library.
")
    (description
      "The `AsyncRead`, `AsyncWrite`, `AsyncSeek`, and `AsyncBufRead` traits for the
futures-rs library.")
    (license (list license:expat license:asl2.0))))

(define-public rust-futures-core-0.3
  (package
    (name "rust-futures-core")
    (version "0.3.21")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "futures-core" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1lqhc6mqklh5bmkpr77p42lqwjj8gaskk5ba2p3kl1z4nw2gs28c"))))
    (build-system cargo-build-system)
    (home-page "https://rust-lang.github.io/futures-rs")
    (synopsis "The core traits and types in for the `futures` library.
")
    (description "The core traits and types in for the `futures` library.")
    (license (list license:expat license:asl2.0))))

(define-public rust-futures-task-0.3
  (package
    (name "rust-futures-task")
    (version "0.3.21")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "futures-task" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0skpiz2ljisywajv79p70yapfwhkqhb39wxy3f09v47mdfbnmijp"))))
    (build-system cargo-build-system)
    (home-page "https://rust-lang.github.io/futures-rs")
    (synopsis "Tools for working with tasks.
")
    (description "Tools for working with tasks.")
    (license (list license:expat license:asl2.0))))

(define-public rust-futures-sink-0.3
  (package
    (name "rust-futures-sink")
    (version "0.3.21")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "futures-sink" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0s58gx5yw1a21xviw2qgc0wzk225vgn4kbzddrp141m3kw9kw5i1"))))
    (build-system cargo-build-system)
    (home-page "https://rust-lang.github.io/futures-rs")
    (synopsis "The asynchronous `Sink` trait for the futures-rs library.
")
    (description "The asynchronous `Sink` trait for the futures-rs library.")
    (license (list license:expat license:asl2.0))))

(define-public rust-futures-channel-0.3
  (package
    (name "rust-futures-channel")
    (version "0.3.21")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "futures-channel" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0420lz2fmxa356ax1rp2sqi7b27ykfhvq4w9f1sla4hlp7j3q263"))))
    (build-system cargo-build-system)
    (arguments
      `(#:cargo-inputs
        (("rust-futures-core" ,rust-futures-core-0.3)
         ("rust-futures-sink" ,rust-futures-sink-0.3))))
    (home-page "https://rust-lang.github.io/futures-rs")
    (synopsis "Channels for asynchronous communication using futures-rs.
")
    (description "Channels for asynchronous communication using futures-rs.")
    (license (list license:expat license:asl2.0))))

(define-public rust-futures-util-0.3
  (package
    (name "rust-futures-util")
    (version "0.3.21")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "futures-util" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0sh3wqi8p36csjffy0irq8nlx9shqxp7z4dsih6bknarsvaspdyq"))))
    (build-system cargo-build-system)
    (arguments
      `(#:cargo-inputs
        (("rust-futures" ,rust-futures-0.1)
         ("rust-futures-channel" ,rust-futures-channel-0.3)
         ("rust-futures-core" ,rust-futures-core-0.3)
         ("rust-futures-io" ,rust-futures-io-0.3)
         ("rust-futures-macro" ,rust-futures-macro-0.3)
         ("rust-futures-sink" ,rust-futures-sink-0.3)
         ("rust-futures-task" ,rust-futures-task-0.3)
         ("rust-memchr" ,rust-memchr-2)
         ("rust-pin-project-lite" ,rust-pin-project-lite-0.2)
         ("rust-pin-utils" ,rust-pin-utils-0.1)
         ("rust-slab" ,rust-slab-0.4)
         ("rust-tokio-io" ,rust-tokio-io-0.1))
        #:cargo-development-inputs
        (("rust-tokio" ,rust-tokio-0.1))))
    (home-page "https://rust-lang.github.io/futures-rs")
    (synopsis
      "Common utilities and extension traits for the futures-rs library.
")
    (description
      "Common utilities and extension traits for the futures-rs library.")
    (license (list license:expat license:asl2.0))))

(define-public rust-jsonrpc-core-18
  (package
    (name "rust-jsonrpc-core")
    (version "18.0.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "jsonrpc-core" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1sv5m6bxyscdqg8cfzlsm8f3vks3972zc9w475l4h19dxxmggxql"))))
    (build-system cargo-build-system)
    (arguments
      `(#:cargo-inputs
        (("rust-futures" ,rust-futures-0.3)
         ("rust-futures-executor" ,rust-futures-executor-0.3)
         ("rust-futures-util" ,rust-futures-util-0.3)
         ("rust-log" ,rust-log-0.4)
         ("rust-serde" ,rust-serde-1)
         ("rust-serde-derive" ,rust-serde-derive-1)
         ("rust-serde-json" ,rust-serde-json-1))))
    (home-page "https://github.com/paritytech/jsonrpc")
    (synopsis
      "Transport agnostic Rust implementation of the JSON-RPC 2.0 Specification.")
    (description
      "This package provides a transport agnostic Rust implementation of the JSON-RPC 2.0 Specification.")
    (license license:expat)))

(define-public rust-libc-0.2
  (package
    (name "rust-libc")
    (version "0.2.118")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "libc" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "153ax7fccbf0wpmhbccgxlbbgcpjfvqzk1xa0i1m18354ikhkr86"))))
    (build-system cargo-build-system)
    (arguments
      `(#:cargo-inputs
        (("rust-rustc-std-workspace-core" ,rust-rustc-std-workspace-core-1))))
    (home-page "https://github.com/rust-lang/libc")
    (synopsis "Raw FFI bindings to platform libraries like libc.
")
    (description "Raw FFI bindings to platform libraries like libc.")
    (license (list license:expat license:asl2.0))))

(define-public rust-socket2-0.4
  (package
    (name "rust-socket2")
    (version "0.4.4")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "socket2" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1q71bsw7sqr3nq71gszywgymxxfv311a3w1aia4k5binjisjpmv6"))))
    (build-system cargo-build-system)
    (arguments
      `(#:cargo-inputs
        (("rust-libc" ,rust-libc-0.2) ("rust-winapi" ,rust-winapi-0.3))))
    (home-page "https://github.com/rust-lang/socket2")
    (synopsis
      "Utilities for handling networking sockets with a maximal amount of configuration
possible intended.
")
    (description
      "Utilities for handling networking sockets with a maximal amount of configuration
possible intended.")
    (license (list license:expat license:asl2.0))))

(define-public rust-lock-api-0.4
  (package
    (name "rust-lock-api")
    (version "0.4.6")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "lock-api" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0frbbqqiwngg33xrc69xagi4rqqk62msllr7z95mlbjaxzbkv548"))))
    (build-system cargo-build-system)
    (arguments
      `(#:cargo-inputs
        (("rust-owning-ref" ,rust-owning-ref-0.4)
         ("rust-scopeguard" ,rust-scopeguard-1)
         ("rust-serde" ,rust-serde-1))))
    (home-page "https://github.com/Amanieu/parking_lot")
    (synopsis
      "Wrappers to create fully-featured Mutex and RwLock types. Compatible with no_std.")
    (description
      "Wrappers to create fully-featured Mutex and RwLock types.  Compatible with
no_std.")
    (license (list license:asl2.0 license:expat))))

(define-public rust-redox-syscall-0.2
  (package
    (name "rust-redox-syscall")
    (version "0.2.10")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "redox_syscall" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1zq36bhw4c6xig340ja1jmr36iy0d3djp8smsabxx71676bg70w3"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-bitflags" ,rust-bitflags-1))))
    (home-page "https://gitlab.redox-os.org/redox-os/syscall")
    (synopsis "A Rust library to access raw Redox system calls")
    (description
      "This package provides a Rust library to access raw Redox system calls")
    (license license:expat)))

(define-public rust-parking-lot-core-0.9
  (package
    (name "rust-parking-lot-core")
    (version "0.9.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "parking-lot-core" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0m49xlpxyw0c65c3011zvgzn2slpviw494816d2a4g8lqh61w518"))))
    (build-system cargo-build-system)
    (arguments
      `(#:cargo-inputs
        (("rust-backtrace" ,rust-backtrace-0.3)
         ("rust-cfg-if" ,rust-cfg-if-1)
         ("rust-libc" ,rust-libc-0.2)
         ("rust-petgraph" ,rust-petgraph-0.5)
         ("rust-redox-syscall" ,rust-redox-syscall-0.2)
         ("rust-smallvec" ,rust-smallvec-1)
         ("rust-thread-id" ,rust-thread-id-4)
         ("rust-windows-sys" ,rust-windows-sys-0.32))))
    (home-page "https://github.com/Amanieu/parking_lot")
    (synopsis
      "An advanced API for creating custom synchronization primitives.")
    (description
      "An advanced API for creating custom synchronization primitives.")
    (license (list license:asl2.0 license:expat))))

(define-public rust-parking-lot-0.12
  (package
    (name "rust-parking-lot")
    (version "0.12.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "parking-lot" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0n7gp0cnfghglc370cxhawwfijvhj3wrjh8gdi8c06m6jcjfrxc7"))))
    (build-system cargo-build-system)
    (arguments
      `(#:cargo-inputs
        (("rust-lock-api" ,rust-lock-api-0.4)
         ("rust-parking-lot-core" ,rust-parking-lot-core-0.9))
        #:cargo-development-inputs
        (("rust-bincode" ,rust-bincode-1) ("rust-rand" ,rust-rand-0.8))))
    (home-page "https://github.com/Amanieu/parking_lot")
    (synopsis
      "More compact and efficient implementations of the standard synchronization primitives.")
    (description
      "More compact and efficient implementations of the standard synchronization
primitives.")
    (license (list license:asl2.0 license:expat))))

(define-public rust-signal-hook-0.3
  (package
    (name "rust-signal-hook")
    (version "0.3.13")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "signal-hook" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0bdzvax49hm0d3s59j062cznhbkznpylzdsv93mdq1qh4zgrfz34"))))
    (build-system cargo-build-system)
    (arguments
      `(#:cargo-inputs
        (("rust-cc" ,rust-cc-1)
         ("rust-libc" ,rust-libc-0.2)
         ("rust-signal-hook-registry" ,rust-signal-hook-registry-1))
        #:cargo-development-inputs
        (("rust-serial-test" ,rust-serial-test-0.5))))
    (home-page "https://github.com/vorner/signal-hook")
    (synopsis "Unix signal handling")
    (description "Unix signal handling")
    (license (list license:asl2.0 license:expat))))

(define-public rust-mio-aio-0.6
  (package
    (name "rust-mio-aio")
    (version "0.6.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "mio-aio" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1z7s0swv1pgvzmn8gaj7cdgid75y3bcklcyqc2b9ihsvxpc6wcca"))))
    (build-system cargo-build-system)
    (arguments
      `(#:cargo-inputs
        (("rust-mio" ,rust-mio-0.7) ("rust-nix" ,rust-nix-0.22))
        #:cargo-development-inputs
        (("rust-assert-impl" ,rust-assert-impl-0.1)
         ("rust-log" ,rust-log-0.3)
         ("rust-mio" ,rust-mio-0.7)
         ("rust-sysctl" ,rust-sysctl-0.1)
         ("rust-tempfile" ,rust-tempfile-3))))
    (home-page "https://github.com/asomers/mio-aio")
    (synopsis "POSIX AIO bindings for mio
")
    (description "POSIX AIO bindings for mio")
    (license (list license:expat license:asl2.0))))

(define-public rust-loom-0.5
  (package
    (name "rust-loom")
    (version "0.5.4")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "loom" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "02a30cv9l2afjq5bg42hgcjspx8fgwyij0cf9saw8b73539wgigd"))))
    (build-system cargo-build-system)
    (arguments
      `(#:cargo-inputs
        (("rust-cfg-if" ,rust-cfg-if-1)
         ("rust-generator" ,rust-generator-0.7)
         ("rust-pin-utils" ,rust-pin-utils-0.1)
         ("rust-scoped-tls" ,rust-scoped-tls-1)
         ("rust-serde" ,rust-serde-1)
         ("rust-serde-json" ,rust-serde-json-1)
         ("rust-tracing" ,rust-tracing-0.1)
         ("rust-tracing-subscriber" ,rust-tracing-subscriber-0.3))
        #:cargo-development-inputs
        (("rust-futures-util" ,rust-futures-util-0.3))))
    (home-page "https://github.com/tokio-rs/loom")
    (synopsis "Permutation testing for concurrent code")
    (description "Permutation testing for concurrent code")
    (license license:expat)))

(define-public rust-clipboard-win-4
  (package
    (name "rust-clipboard-win")
    (version "4.4.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "clipboard-win" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1nq58r8znx92k2yl9qhylp1z8pjfw6n9vfqw3q41zh1d2cw14gig"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs
        (("rust-error-code" ,rust-error-code-2)
         ("rust-str-buf" ,rust-str-buf-1)
         ("rust-winapi" ,rust-winapi-0.3))))
    (home-page "https://github.com/DoumanAsh/clipboard-win")
    (synopsis "Provides simple way to interact with Windows clipboard.")
    (description
      "This package provides simple way to interact with Windows clipboard.")
    (license license:boost1.0)))

(define-public rust-crossterm-0.23
  (package
    (name "rust-crossterm")
    (version "0.23.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "crossterm" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0cmlsf9r38cs9mi3mfqf40574kd3ap6nksi1hlghy8ldvhkmmdvp"))))
    (build-system cargo-build-system)
    (arguments
      `(#:cargo-inputs
        (("rust-bitflags" ,rust-bitflags-1)
         ("rust-crossterm-winapi" ,rust-crossterm-winapi-0.9)
         ("rust-futures-core" ,rust-futures-core-0.3)
         ("rust-libc" ,rust-libc-0.2)
         ("rust-mio" ,rust-mio-0.7)
         ("rust-parking-lot" ,rust-parking-lot-0.12)
         ("rust-serde" ,rust-serde-1)
         ("rust-signal-hook" ,rust-signal-hook-0.3)
         ("rust-signal-hook-mio" ,rust-signal-hook-mio-0.2)
         ("rust-winapi" ,rust-winapi-0.3))
        #:cargo-development-inputs
        (("rust-async-std" ,rust-async-std-1)
         ("rust-futures" ,rust-futures-0.3)
         ("rust-futures-timer" ,rust-futures-timer-3)
         ("rust-serde-json" ,rust-serde-json-1)
         ("rust-tokio" ,rust-tokio-1))))
    (home-page "https://github.com/crossterm-rs/crossterm")
    (synopsis "A crossplatform terminal library for manipulating terminals.")
    (description
      "This package provides a crossplatform terminal library for manipulating
terminals.")
    (license license:expat)))

(define-public rust-unicode-segmentation-1
  (package
    (name "rust-unicode-segmentation")
    (version "1.9.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "unicode-segmentation" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "16gxxda9aya0arcqs9aa9lb31b3i54i34dmyqi6j5xkpszsj123y"))))
    (build-system cargo-build-system)
    (arguments
      `(#:cargo-development-inputs
        (("rust-criterion" ,rust-criterion-0.3)
         ("rust-quickcheck" ,rust-quickcheck-0.7))))
    (home-page "https://github.com/unicode-rs/unicode-segmentation")
    (synopsis
      "This crate provides Grapheme Cluster, Word and Sentence boundaries
according to Unicode Standard Annex #29 rules.
")
    (description
      "This crate provides Grapheme Cluster, Word and Sentence boundaries according to
Unicode Standard Annex #29 rules.")
    (license (list license:expat license:asl2.0))))

(define-public rust-unicode-general-category-0.5
  (package
    (name "rust-unicode-general-category")
    (version "0.5.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "unicode-general-category" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1mnz1haq2wf7knyln6ff8z992r4p1p3h8hc2l8cmn25qd220j60j"))))
    (build-system cargo-build-system)
    (home-page "https://github.com/yeslogic/unicode-general-category")
    (synopsis "Fast lookup of the Unicode General Category property for char")
    (description
      "Fast lookup of the Unicode General Category property for char")
    (license license:asl2.0)))

(define-public rust-lsp-types-0.92
  (package
    (name "rust-lsp-types")
    (version "0.92.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "lsp-types" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0xpw622gsl3js356rap7ah1z7kkz3a5sdkn3ky6206ym890rv9p8"))))
    (build-system cargo-build-system)
    (arguments
      `(#:cargo-inputs
        (("rust-bitflags" ,rust-bitflags-1)
         ("rust-serde" ,rust-serde-1)
         ("rust-serde-json" ,rust-serde-json-1)
         ("rust-serde-repr" ,rust-serde-repr-0.1)
         ("rust-url" ,rust-url-2))))
    (home-page "https://github.com/gluon-lang/lsp-types")
    (synopsis
      "Types for interaction with a language server, using VSCode's Language Server Protocol")
    (description
      "Types for interaction with a language server, using VSCode's Language Server
Protocol")
    (license license:expat)))

(define-public rust-encoding-rs-0.8
  (package
    (name "rust-encoding-rs")
    (version "0.8.30")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "encoding_rs" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1pqirqhlj8mbaln0pv4dk65yr22clpx509ci6gdgs3r5pf5dr5kq"))))
    (build-system cargo-build-system)
    (arguments
      `(#:cargo-inputs
        (("rust-cfg-if" ,rust-cfg-if-1)
         ("rust-packed-simd-2" ,rust-packed-simd-2-0.3)
         ("rust-serde" ,rust-serde-1))
        #:cargo-development-inputs
        (("rust-bincode" ,rust-bincode-1)
         ("rust-serde-derive" ,rust-serde-derive-1)
         ("rust-serde-json" ,rust-serde-json-1))))
    (home-page "https://docs.rs/encoding_rs/")
    (synopsis "A Gecko-oriented implementation of the Encoding Standard")
    (description
      "This package provides a Gecko-oriented implementation of the Encoding Standard")
    (license (list ;; the correct spdx: ((Apache-2.0 OR MIT) AND BSD-3-Clause)
	      license:asl2.0 license:expat license:bsd-3))))

(define-public rust-tokio-stream-0.1.8
  (package
    (name "rust-tokio-stream")
    (version "0.1.8")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "tokio-stream" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1qwq0y21xprsql4v9y1cm1ymhgw66rznjmnjrjsii27zxy25852h"))))
    (build-system cargo-build-system)
    (arguments
      `(#:cargo-inputs
        (("rust-futures-core" ,rust-futures-core-0.3)
         ("rust-pin-project-lite" ,rust-pin-project-lite-0.2)
         ("rust-tokio" ,rust-tokio-1)
         ("rust-tokio-util" ,rust-tokio-util-0.6))
        #:cargo-development-inputs
        (("rust-async-stream" ,rust-async-stream-0.3)
         ("rust-futures" ,rust-futures-0.3)
         ("rust-proptest" ,rust-proptest-1)
         ("rust-tokio" ,rust-tokio-1))))
    (home-page "https://tokio.rs")
    (synopsis "Utilities to work with `Stream` and `tokio`.
")
    (description "Utilities to work with `Stream` and `tokio`.")
    (license license:expat)))

(define-public rust-tokio-1
  (package
    (name "rust-tokio")
    (version "1.17.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "tokio" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1vm5ynzjpsqzqv15fdrk69n6y8fhwlilmqvj270ggwsnjz23mxra"))))
    (build-system cargo-build-system)
    (arguments
      `(#:cargo-inputs
        (("rust-bytes" ,rust-bytes-1)
         ("rust-libc" ,rust-libc-0.2)
         ("rust-memchr" ,rust-memchr-2)
         ("rust-mio" ,rust-mio-0.8)
         ("rust-num-cpus" ,rust-num-cpus-1)
         ("rust-once-cell" ,rust-once-cell-1)
         ("rust-parking-lot" ,rust-parking-lot-0.12)
         ("rust-pin-project-lite" ,rust-pin-project-lite-0.2)
         ("rust-signal-hook-registry" ,rust-signal-hook-registry-1)
         ("rust-socket2" ,rust-socket2-0.4)
         ("rust-tokio-macros" ,rust-tokio-macros-1)
         ("rust-tracing" ,rust-tracing-0.1)
         ("rust-winapi" ,rust-winapi-0.3))
        #:cargo-development-inputs
        (("rust-async-stream" ,rust-async-stream-0.3)
         ("rust-futures" ,rust-futures-0.3)
         ("rust-libc" ,rust-libc-0.2)
         ("rust-loom" ,rust-loom-0.5)
         ("rust-mio-aio" ,rust-mio-aio-0.6)
         ("rust-mockall" ,rust-mockall-0.10)
         ("rust-nix" ,rust-nix-0.23)
         ("rust-ntapi" ,rust-ntapi-0.3)
         ("rust-proptest" ,rust-proptest-1)
         ("rust-rand" ,rust-rand-0.8)
         ("rust-socket2" ,rust-socket2-0.4)
         ("rust-tempfile" ,rust-tempfile-3)
         ("rust-tokio-stream" ,rust-tokio-stream-0.1)
         ("rust-tokio-test" ,rust-tokio-test-0.4)
         ("rust-wasm-bindgen-test" ,rust-wasm-bindgen-test-0.3))))
    (home-page "https://tokio.rs")
    (synopsis
      "An event-driven, non-blocking I/O platform for writing asynchronous I/O
backed applications.
")
    (description
      "An event-driven, non-blocking I/O platform for writing asynchronous I/O backed
applications.")
    (license license:expat)))

(define-public rust-pulldown-cmark-0.9
  (package
    (name "rust-pulldown-cmark")
    (version "0.9.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "pulldown-cmark" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1ml1l6adgsmpxipjcm3hz5xmrfybr5z9ldbcwhxapjdh8jjrgw9l"))))
    (build-system cargo-build-system)
    (arguments
      `(#:cargo-inputs
        (("rust-bitflags" ,rust-bitflags-1)
         ("rust-getopts" ,rust-getopts-0.2)
         ("rust-memchr" ,rust-memchr-2)
         ("rust-serde" ,rust-serde-1)
         ("rust-unicase" ,rust-unicase-2))
        #:cargo-development-inputs
        (("rust-bincode" ,rust-bincode-1)
         ("rust-criterion" ,rust-criterion-0.3)
         ("rust-html5ever" ,rust-html5ever-0.25)
         ("rust-lazy-static" ,rust-lazy-static-1)
         ("rust-markup5ever-rcdom" ,rust-markup5ever-rcdom-0.1)
         ("rust-regex" ,rust-regex-1)
         ("rust-serde-json" ,rust-serde-json-1)
         ("rust-tendril" ,rust-tendril-0.4))))
    (home-page "https://github.com/raphlinus/pulldown-cmark")
    (synopsis "A pull parser for CommonMark")
    (description "This package provides a pull parser for CommonMark")
    (license license:expat)))
