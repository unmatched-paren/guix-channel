;; SPDX-License-Identifier: GPL-3.0-or-later
;; See <https://spdx.org/licenses>.
;; Some code may be lifted from GNU Guix itself;
;; see the files in <https://git.sv.gnu.org/cgit/guix.git/tree>
;; for the original attributions.

(define-module (paren packages wm)
  #:use-module (guix build-system meson)
  #:use-module (guix build-system python)
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix packages)
  #:use-module (gnu packages freedesktop)
  #:use-module (gnu packages gtk)
  #:use-module (gnu packages man)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages python-build)
  #:use-module (gnu packages python-xyz))

(define-public python-i3ipc
  (package
   (name "python-i3ipc")
   (version "2.2.1")
   (source
    (origin
     (method url-fetch)
     (uri (pypi-uri "i3ipc" version))
     (sha256
      (base32 "1s6crkdn7q8wmzl5d0pb6rdkhhbvp444yxilrgaylnbr2kbxg078"))))
   (build-system python-build-system)
   (arguments
    ;; TODO: Fix tests
    '(#:tests? #f))
   (propagated-inputs (list python-xlib))
   (home-page "https://github.com/altdesktop/i3ipc-python")
   (synopsis "An improved Python library to control i3wm and sway")
   (description "This package provides an improved Python library to control i3wm and sway.")
   (license license:bsd-3)))

(define-public i3-autotiling
  (package
   (name "i3-autotiling")
   (version "1.6")
   (source
    (origin
     (method git-fetch)
     (uri (git-reference
	   (url "https://github.com/nwg-piotr/autotiling")
	   (commit (string-append "v" version))))
     (file-name (git-file-name name version))
     (sha256
      (base32
       "1hjlvg7095s322gb43r9g7mqlsy3pj13l827jpnbn5x0918rq9rr"))))
   (build-system python-build-system)
   (arguments
    ;; TODO: Fix tests
    '(#:tests? #f))
   (propagated-inputs
    (list python-i3ipc python-setuptools python-wheel))
   (home-page "https://github.com/nwg-piotr/autotiling")
   (synopsis "Automatic layout tiling for i3wm and sway.")
   (description "This package uses @code{python-i3ipc} to control the tiling of i3wm and sway. It automatically switches between horizontal and vertical layouts to make the best use of space without human intervention.")
   (license license:gpl3+)))