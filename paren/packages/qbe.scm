
(define-module (paren packages qbe)
  #:use-module (guix build-system gnu)
  #:use-module (guix git-download)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix packages))

(define-public qbe
  (let ((commit "e092473be1cf48216085f8349677cefc8f19b3b2")
		(revision "0"))
    (package
	  (name "qbe")
	  (version (git-version "0.0.0" revision commit))
	  (source
		(origin
		  (method git-fetch)
		  (uri (git-reference
				 (url "https://codeberg.org/unmatched-paren/qbe")
				 (commit commit)))
		  (sha256 
			(base32 "1vrrrmlxixwlrk7x64lky8pn924idy3yw42sxdvpg3bkcjjsf426"))))
	  (build-system gnu-build-system)
	  (arguments
		`(#:make-flags
		  (list "CC=gcc")
		  #:phases
		  (modify-phases %standard-phases
			(delete 'configure)
			(add-after 'unpack 'patch-test-sh
			  (lambda _
				(substitute* "tools/test.sh"
				  (("\"cc") "\"gcc"))))
			(replace 'install
			  (lambda* (#:key outputs #:allow-other-keys)
				(let ((out (assoc-ref outputs "out")))
				  (install-file "obj/qbe" (string-append out "/bin"))))))))
	  (home-page "https://c9x.me/compile")
	  (synopsis "QBE is a compiler backend that provides 70% of the performance of advanced compilers in 10% of the code. ")
	  (description "QBE aims to be a pure C embeddable backend that provides 70% of the performance of advanced compilers in 10% of the code. 

Its small size serves both its aspirations of correctness and our ability to understand, fix, and improve it. It also serves its users by providing trivial integration and great flexibility.")
	  (license license:expat))))

qbe
