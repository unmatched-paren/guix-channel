;; SPDX-License-Identifier: GPL-3.0-or-later
;; See <https://spdx.org/licenses>.
;; Some code may be lifted from GNU Guix itself;
;; see the files in <https://git.sv.gnu.org/cgit/guix.git/tree>
;; for the original attributions.

(define-module (paren packages helix)
  #:use-module (guix build-system cargo)
  #:use-module (guix download) ;; TODO remove
  #:use-module (guix git-download)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix packages)
  #:use-module (gnu packages crates-io)
  #:use-module (paren packages crates-io)
  #:use-module (paren packages tree-sitter))

(define-public helix
  (let ((commit "d11b6521399f41cb4c0b75ebaa5770972f550ebb"))
    (package
     (name "helix")
     (version (git-version "0.6.0" "0" commit))
     (source
      (origin
       (method git-fetch)
       (uri (git-reference
	     (commit commit)
	     (url "https://github.com/helix-editor/helix")
	     ;; Helix pulls in Tree-sitter grammars in its submodules.
	     (recursive? #t)))
       (modules '((guix build utils)))
       (snippet '(begin
		   ;; Fix the `tui` dependency so that the `package` stage doesn't complain.
		   (substitute* "helix-term/Cargo.toml"
			        (("tui = \\{ path = \"../helix-tui\", package = \"helix-tui\", default-features = false, features = \\[\"crossterm\"\\] \\}")
				 "tui = { version = \"*\", path = \"../helix-tui\", package = \"helix-tui\", default-features = false, features = [\"crossterm\"] }"))
		   ;; 
		   (substitute* "helix-lsp/Cargo.toml"
			        (("futures-util = \\{ version = \"0.3\", features = \\[\"std\", \"async-await\"\\], default-features = false \\}")
				 "futures-util = { version = \"0.3\", features = [\"std\", \"alloc\", \"async-await\"], default-features = false }"))
		   ;; Fix the other local deps to stop `package` complaining some more.
		   (delete-file-recursively "helix-syntax/languages/tree-sitter-yaml/yaml-test-suite")))
       (sha256
        (base32 "1xpa8zf7abwri6wyxb90nvqi3sqjkvmy7dllxairv07x6csrvsgi"))))
     (build-system cargo-build-system)
     (arguments
      `(#:tests? #f
	#:cargo-inputs
	(("rust-anyhow" ,rust-anyhow-1)
	 ("rust-arc-swap" ,rust-arc-swap-1)
	 ("rust-autocfg" ,rust-autocfg-1)
	 ("rust-bitflags" ,rust-bitflags-1)
	 ("rust-cassowary" ,rust-cassowary-0.3)
	 ("rust-cc" ,rust-cc-1)
	 ("rust-chardetng" ,rust-chardetng-0.1)
	 ("rust-chrono" ,rust-chrono-0.4)
	 ("rust-clipboard-win" ,rust-clipboard-win-4)
	 ("rust-content-inspector" ,rust-content-inspector-0.2)
	 ("rust-crossterm" ,rust-crossterm-0.23)
	 ("rust-encoding" ,rust-encoding-rs-0.8)
	 ("rust-etcetera" ,rust-etcetera-0.3)
	 ("rust-futures-executor" ,rust-futures-executor-0.3)
	 ("rust-futures-util" ,rust-futures-util-0.3)
	 ("rust-fuzzy-matcher" ,rust-fuzzy-matcher-0.3)
	 ("rust-grep-regex" ,rust-grep-regex-0.1)
	 ("rust-grep-searcher" ,rust-grep-searcher-0.1)
	 ("rust-ignore" ,rust-ignore-0.4)
	 ("rust-jsonrpc-core" ,rust-jsonrpc-core-18)
	 ("rust-libloading" ,rust-libloading-0.7)
	 ("rust-log" ,rust-log-0.4)
	 ("rust-lsp-types" ,rust-lsp-types-0.92)
	 ("rust-num-cpus" ,rust-num-cpus-1)
	 ("rust-once-cell" ,rust-once-cell-1)
	 ("rust-pulldown-cmark" ,rust-pulldown-cmark-0.9)
	 ("rust-regex" ,rust-regex-1)
	 ("rust-ropey" ,rust-ropey-1.3)
	 ("rust-serde" ,rust-serde-1)
	 ("rust-serde-json" ,rust-serde-json-1)
	 ("rust-signal-hook" ,rust-signal-hook-0.3)
	 ("rust-signal-hook-tokio" ,rust-signal-hook-tokio-0.3)
	 ("rust-similar" ,rust-similar-2)
	 ("rust-slotmap" ,rust-slotmap-1)
	 ("rust-smallvec" ,rust-smallvec-1.8)
	 ("rust-smartstring" ,rust-smartstring-0.2)
	 ("rust-thiserror" ,rust-thiserror-1)
	 ("rust-threadpool" ,rust-threadpool-1)
	 ("rust-tokio" ,rust-tokio-1)
	 ("rust-tokio-stream" ,rust-tokio-stream-0.1.8)
	 ("rust-toml" ,rust-toml-0.5)
	 ("rust-tree-sitter" ,rust-tree-sitter-0.20)
	 ("rust-unicode-general-category" ,rust-unicode-general-category-0.5)
	 ("rust-unicode-segmentation" ,rust-unicode-segmentation-1)
	 ("rust-unicode-width" ,rust-unicode-width-0.1)
	 ("rust-url" ,rust-url-2)
	 ("rust-which" ,rust-which-4))
	#:cargo-development-inputs
	(("rust-fern" ,rust-fern-0.6)
         ("rust-quickcheck" ,rust-quickcheck-1))
	#:phases
	(modify-phases %standard-phases
		       (add-after 'build 'gross-hack
				  (lambda _
                                    (substitute* "helix-dap/Cargo.toml"
				      (("helix-(.*) = \\{ version = \"(.*)\", path = \"../helix-(.*)\" \\}" _ pkg-name)
				       ""))
                                    (substitute* "helix-lsp/Cargo.toml"
				      (("helix-(.*) = \\{ version = \"(.*)\", path = \"../helix-(.*)\" \\}" _ pkg-name)
				       ""))
                                    (substitute* "helix-syntax/Cargo.toml"
				      (("helix-(.*) = \\{ version = \"(.*)\", path = \"../helix-(.*)\" \\}" _ pkg-name)
				       ""))
				    (substitute* "helix-term/Cargo.toml"
				      (("helix-(.*) = \\{ version = \"(.*)\", path = \"../helix-(.*)\" \\}" _ pkg-name)
				       ""))
                                    (substitute* "helix-tui/Cargo.toml"
				      (("helix-(.*) = \\{ version = \"(.*)\", path = \"../helix-(.*)\" \\}" _ pkg-name)
				       ""))
                                    (substitute* "helix-view/Cargo.toml"
				      (("helix-(.*) = \\{ version = \"(.*)\", path = \"../helix-(.*)\" \\}" _ pkg-name)
				       ""))
                                    (substitute* "xtask/Cargo.toml"
				      (("helix-(.*) = \\{ version = \"(.*)\", path = \"../helix-(.*)\" \\}" _ pkg-name)
				       ""))
				    (substitute* "helix-term/Cargo.toml"
						 (("tui = .*\n")
						  ""))))
		       (replace 'install
				(lambda* (#:key outputs #:allow-other-keys)
				  (let ((out (assoc-ref outputs "out")))
				    (install-file "target/release/hx" (string-append out "/bin")))))
		       (add-after 'install 'install-runtime
				  (lambda* (#:key outputs #:allow-other-keys)
				    (let ((out (assoc-ref outputs "out")))
				      (copy-recursively "runtime" (string-append out "/usr/share/helix/runtime")))))
		       (add-after 'install-runtime 'patch-runtime-env-var
				  (lambda* (#:key outputs #:allow-other-keys)
				    (let ((out (assoc-ref outputs "out")))
				      (wrap-program (string-append out "/bin/hx")
						    `("HELIX_RUNTIME" = (,(string-append out "/usr/share/helix/runtime"))))))))))
     (home-page "https://helix-editor.com")
     (synopsis "A post-modern text editor.")
     (description "Helix is a modal terminal-based text editor inspired by
Neovim and Kakoune, with modern features like LSP included by default.")
     (license license:mpl2.0))))
